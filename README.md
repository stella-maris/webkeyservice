# Webkeyservice

Create a web-based WebKeyDirectory for easy distribution of public GPG keys

## Idea

Distributing public gpg-keys is usually done via keyservers. 

As those are append only they contain a lot of keys that are outdated or - which can be more problematic - can contain fake keys for email-addresses that are not approved by the owner.

To allow users to actually retrieve the correct key there is the idea of a WebKeyDirectory which allows retrieval of a public key via a dedicated URL on the emails domain. So the public key for the email-address foo@example.com can be retrieved from a dedicated URL on the server example.com.

There already is the concept of a WebKeyService which allows sending a public key via an encrypted message to a dedicated email-address. Bu tthat requires setup of an email-system which is on shared hosting platforms a rather difficult setup.

So to make sharing public keys via a WebKey Directory easier also for the general public this projcet aims to provide a way to upload a public key via a web-interface that authenticates against the Email-Server (therefore verifying that the user has access to the emails) and then puts the key to the right location.

An add-on would be to have a service that continuously monitors key-servers to update the existing public key from keyserves. That will allow one to publish a key on a key-server and allow other people to sign that key and distribute the published key afterwards without having to upload that key to the own webkeydirectory again.

ALso an addition would be to provide an interface that shows some details of a users public key for verification. Like Fingerprint or expiration date.

Checkout https://wiki.gnupg.org/WKD for more informations
